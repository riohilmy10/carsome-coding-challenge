require('./bootstrap');

import InspectionBooking from './components/InspectionBooking'

window.Vue = require('vue');

new Vue({
    el: '#app',
    components: {
        InspectionBooking,
    }
});
