<?php

    return [
        'inspection_date_not_within_3_weeks' => 'Inspection date and time should only be within 3 weeks from today',
        'inspection_time_unavailable' => 'Inspection appointment time is only between 9AM to 6PM',
        'inspection_date_is_fully_booked' => 'The selected inspection appointment date and time has been fully booked, please try another date and time.',
        'duplicate_booking_by_same_user' => 'You are not allowed to book the same inspection appointment date and time twice',
        'create_inspection_booking_success' => 'The selected inspection appointment date and time has been successfully booked.',
        'inspection_date_unavailable_on_sunday' => 'Inspection appointment is unavailable on Sunday',
        'internal_server_error' => 'Something went wrong, please try again later or contact our customer service.',
    ]

?>