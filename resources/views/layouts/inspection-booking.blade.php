@extends('master')

@section('content')
    <inspection-booking
        {{-- Send API routes to Vue component as props --}}
        api-create-inspection-booking-url="{{ route('api.inspection-booking.post.create') }}"
        api-get-inspection-booking-unavailable-time-slots-url="{{ route('api.inspection-booking.get.unavailable-time-slots') }}"
    ></inspection-booking>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endsection
