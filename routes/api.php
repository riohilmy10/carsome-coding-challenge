<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    ['namespace' => 'Api', 'as' => 'api.'],
    function () {
        Route::group(
            ['prefix' => 'inspection-booking', 'as' => 'inspection-booking.'],
            function () {
                Route::post('/', 'InspectionBookingApiController@create')->name('post.create');
                Route::get('/unavailable-time-slots', 'InspectionBookingApiController@getUnavailableTimeSlotsByDate')->name('get.unavailable-time-slots');
            }
        );
    }
);
