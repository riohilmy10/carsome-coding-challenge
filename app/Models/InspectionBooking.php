<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InspectionBooking extends Model
{
    protected $fillable = [
        'booked_at',
        'is_saturday',
        'user_id',
    ];
}
