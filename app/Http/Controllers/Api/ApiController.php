<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs;

    public function formatSuccessResponse($message, $http_code = 200)
    {
        $response = [
            'status_code' => $http_code,
            'message' => $message
        ];

        return response()->json($response, $http_code);
    }

    public function formatErrorResponse($error_message, $http_code)
    {
        $message = [
            'status_code' => $http_code,
            'errors' => $error_message,
        ];

        return response()->json($message, $http_code);
    }

    public function formatResourceResponse($data, $http_code, $message = null)
    {
        $response = [
            'status_code' => $http_code,
            'data' => $data
        ];

        if ($message) {
            $response['message'] = $message;
        }

        return response()->json($response, $http_code);
    }
}
