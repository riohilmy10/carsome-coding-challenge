<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\CreateInspectionBookingRequest;
use App\Http\Requests\GetUnavailableTimeSlotsRequest;
use App\Models\InspectionBooking;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class InspectionBookingApiController extends ApiController
{
    /* API method for public user to book inspection slot */
    public function create(CreateInspectionBookingRequest $request)
    {
        /* I always use try and catch on all of my APIs, as precautions if somehow we missed any possible error case
        or any unexpected things showed up, the errors will be recorded in the log and the API will return the error response properly */
        try {
            /* First checking, check if the date is within next 3 weeks
               If following the flow from UI form, below error will never happen as I disabled
               dates and times that has been fully booked or not within 3 weeks or outside 9AM to 6PM
               But backend API should expect request coming from other sources which flow might be different
               Hence I create all possible error cases, this is my best practice on creating APIs */
            $today = Carbon::today();
            $next_three_week = Carbon::today()->addWeeks(3)->addDay();
            $booking_date = Carbon::parse($request->booking_date); // Parse requested date to be Carbon object

            // Return error if requested date is not within 3 weeks from today
            if (!$booking_date->between($today, $next_three_week)) {
                return $this->formatErrorResponse(
                    // I always store all response messages in \resources\lang\messages.php
                    trans('messages.inspection_date_not_within_3_weeks'),
                    409
                );
            }

            // Second checking, check if the time is not before 9AM and not after 6PM
            $start_date = Carbon::parse($request->booking_date)->setTime(9, 0, 0);
            $end_date = Carbon::parse($request->booking_date)->setTime(18, 0, 0);

            // Return error if requested date is outside available inspection time
            if ($booking_date->lt($start_date) || $booking_date->gt($end_date)) {
                // Return my custom error API response
                return $this->formatErrorResponse(
                    trans('messages.inspection_time_unavailable'),
                    409
                );
            }

            // Third checking, check how many slots have been booked for the requested date and time
            $inspection_booking = InspectionBooking::where('booked_at', $request->booking_date)->count();

            // Return error if weekdays already have 2 appointment slots, and saturday already have 4 appointment slots
            if (($booking_date->dayOfWeek != Carbon::SATURDAY && $inspection_booking >= 2) ||
                ($booking_date->dayOfWeek == Carbon::SATURDAY && $inspection_booking >= 4)) {
                    // Return my custom error API response
                    return $this->formatErrorResponse(
                        trans('messages.inspection_date_is_fully_booked'),
                        409
                    );
            }

            // Forth checking, return error if requested date is on Sunday
            if ($booking_date->dayOfWeek == Carbon::SUNDAY) {
                    // Return my custom error API response
                    return $this->formatErrorResponse(
                        trans('messages.inspection_date_unavailable_on_sunday'),
                        409
                    );
            }

            // Fifth checking, return error if requested user id has already made booking on the same date and time
            $booking_exists = InspectionBooking::where('booked_at', $request->booking_date)
                ->where('user_id', $request->user_id)
                ->exists();

            if ($booking_exists) {
                // Return my custom error API response
                return $this->formatErrorResponse(
                    trans('messages.duplicate_booking_by_same_user'),
                    409
                );
            }

            // After passing all possible error cases, can proceed to store the data and return success messages
            InspectionBooking::create(
                [
                    'booked_at' => $request->booking_date,
                    'is_saturday' => $booking_date->dayOfWeek == Carbon::SATURDAY ? true : false,
                    'user_id' => $request->user_id
                ]
            );

            // Return my custom success API response
            return $this->formatSuccessResponse(
                trans('messages.create_inspection_booking_success'),
                200
            );
        } catch (\Exception $e) {
            // Write down all unexpected errors to the log
            \Log::error($e);

            // Return general internal server error responses as most of the time, errors coming from exception are unexpected and unknown
            return $this->formatErrorResponse(
                trans('messages.internal_server_error'),
                500
            );
        }
    }
    
    /* API method for front-end to get all unavailable time slots of the requested date */
    public function getUnavailableTimeSlotsByDate(GetUnavailableTimeSlotsRequest $request)
    {
        try {
            // Fetch all datetime that have been fully booked, 2 rows count for weekday, 4 rows count for Saturday
            $unavailable_slots = InspectionBooking::select('booked_at', DB::raw('COUNT("booked_at") as count'))
                ->whereDate('booked_at', $request->booking_date)
                ->groupBy('booked_at', 'is_saturday')
                ->when(
                    Carbon::parse($request->booking_date)->dayOfWeek == Carbon::SATURDAY,
                    function ($query) use ($request) {
                        $query->havingRaw('is_saturday <> 1 or count = 4');
                    }
                )
                ->when(
                    Carbon::parse($request->booking_date)->dayOfWeek != Carbon::SATURDAY,
                    function ($query) use ($request) {
                        $query->havingRaw('is_saturday <> 0 or count = 2');
                    }
                )
                ->get()
                ->toArray();

            // Fetch current user booked date time to avoid same user id booking same date and time
            $current_user_booked_slots = InspectionBooking::select('booked_at')
                ->whereDate('booked_at', $request->booking_date)
                ->where('user_id', $request->user_id)
                ->get()
                ->toArray();

            $unavailable_slots = array_merge($unavailable_slots, $current_user_booked_slots); // Merge fully booked array with user booked array
            $unique_unavailable_slots = array_unique(array_column($unavailable_slots, 'booked_at')); // Remove duplicate booked time that appears in both array

            // Reformat the result for easier front-end consumption
            foreach ($unique_unavailable_slots as $date) {
                $results[] = [
                    'hour' => (int) Carbon::parse($date)->format('H'), // Get only the hour parsed as integer for easier front-end consumption
                    'minute' => (int) Carbon::parse($date)->format('i'), // Get only the minute as integer for easier front-end consumption
                ];
            }

            // Return my custom resource API response
            return $this->formatResourceResponse(
                $results ?? [], // if $result is null (never went to the loop above), return empty array
                200
            );
        } catch (\Exception $e) {
            // Write down all unexpected errors to the log
            \Log::error($e);

            // Return general internal server error responses as most of the time, errors coming from exception are unexpected and unknown
            return $this->formatErrorResponse(
                trans('messages.internal_server_error'),
                500
            );
        }
    }
}
